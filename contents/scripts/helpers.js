'use strict';
var tables = document.querySelectorAll('table');
var copy = function copy(collection) {
    return Array.prototype.slice.call(collection);
};
tables = copy(tables);
tables.forEach(function(item) {
    var thead = copy(item.tHead.rows[0].cells).map(function(item) {
        return item.textContent;
    });
    var tbody = copy(item.tBodies).forEach(function(tbody) {
        return copy(tbody.rows).forEach(function(row) {
            return copy(row.cells).forEach(function(cell, index) {
                return cell.setAttribute('data-title', thead[index]);
            });
        });
    });
});

var updownElem = document.getElementById('updown');
var pageYLabel = 0;

// aditional syntax highlightin'

//

updownElem.onclick = function() {
  var pageY = window.pageYOffset || document.documentElement.scrollTop;

  window.scrollTo(0, pageYLabel);
  this.classList.toggle('show-updown');
}

window.onscroll = function() {
  var pageY = window.pageYOffset || document.documentElement.scrollTop;
  var innerHeight = document.documentElement.clientHeight;

  switch (updownElem.className) {
    case '':
      if (pageY > innerHeight) {
        updownElem.className = 'show-updown';
      }
      break;

    case 'show-updown':
      if (pageY < innerHeight) {
        updownElem.className = '';
      }
      break;

    // case 'down':
    //   if (pageY > innerHeight) {
    //     updownElem.className = 'up';
    //   }
    //   break;
  }
}