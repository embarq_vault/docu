<a name="top"></a>

# Лабораторна робота №1.

## Тема: Скриптова анiмацiя SVG - графiки.

## Мета:

1.  Ознайомитися з об'єктною моделлю SVG - документiв.
2.  Опанувати методику створення документiв у форматi SVG.
3.  Набути досвiд створення SVG - анiмацiї.

## Теоретичнi вiдомостi.

Формат SVG передбачає використання мови програмування ECMAscript - аналогу JavaScript. Ця мова має таку саму об'єктну модель та синтаксис. Суттєво вiдрiзняється лише об'єктна модель SVG - документа.

Мова ECMAscript також є подiйно - орiєнтованою.

### Подiї, що визначенi у SVG.

| Подiї,
що пов'язанi з елемаентами
графiки i контейнерами: | Подiї
рiвня документу: | Подiї
рiвня анiмацiї: |
| 

*   onactivate
*   onclick
*   onfocusin
*   onfocusout
*   onload
*   onmousedown
*   onmousemove
*   onmouseout
*   onmouseover
*   onmouseup

 | 

*   onunload
*   onabort
*   onerror
*   onresize
*   onscroll
*   onzoom

 | 

*   onbegin
*   onend
*   onrepeat
*   onload

 |

### Приклад анiмацiї циклiчного руху.

| `<svg xmlns="http://www.w3.org/2000/svg" onload="StartAnimation( evt)" width="320" height="240" encoding="utf-8">
  <script type="text/ecmascript">
    <![CDATA[
      var pos;
      var t;
      var big_dot;

      function StartAnimation( evt) {
        pos = 0;
        t = 0;
        big_dot = document.getElementById( "big_dot");
        motion();
        }

      function motion() {
        pos = 100 * Math.sin( t) + 160.0;
        t += 0.1;
        big_dot.setAttributeNS( null, "transform", "translate(" + pos + ", 120)");
        setTimeout( "motion()", 30);
        }
    ]]>
  </script>

  <title>Cycle Motion</title>
  <g id="body">
    <g id="big_dot" stroke="red">
      <circle cx="0" cy="0" r="20" fill="yellow" stroke="red" stroke-width="5" />
    </g>
  </g>
</svg>` |

### Приклад анiмацiї простого обертання.

| `<svg xmlns="http://www.w3.org/2000/svg" **onload**="StartAnimation(evt)" width="320" height="240" encoding="utf-8">
  <script type="text/ecmascript">
    <![CDATA[
      var rot = 0;
      var sun;

      function StartAnimation( evt) {
        rot = 0;
        sun = document.getElementById( "sun");
        motion();
        }

      function motion() {
        rot += 1;
        sun.setAttributeNS( null, "transform", "translate(160, 120), rotate(" + rot + ")");
        setTimeout( "motion()", 30);
        }
    ]]>
  </script>

  <title>Sun Rotation</title>
  <g id="body">
    <g id="sun" stroke="red" transform="translate(160, 120)">
      <line x1="-30" y1="-30" x2="30" y2="30" stroke-width="5" />
      <line x1="-30" y1="30" x2="30" y2="-30" stroke-width="5" />
      <line x1="-30" y1="0" x2="30" y2="0" stroke-width="5" />
      <line x1="0" y1="30" x2="0" y2="-30" stroke-width="5" />
      <circle cx="0" cy="0" r="20" fill="yellow" stroke="red" stroke-width="5" />
    </g>
  </g>
</svg>` |

### Простий цифровий годинник.

| `<svg xmlns="http://www.w3.org/2000/svg" **onload**="StartAnimation( evt)" width="200" height="70" encoding="utf-8">
  <script type="text/ecmascript">
    <![CDATA[
      var timeField;

      function StartAnimation( evt) {
        timeField = document.getElementById( "timeField");
        motion();
        }

      function leadingZero( s) {
        if (s < 10) {
          s = "0" + s;
          }
        return( s);
        }

      function updateText( id, lbl) {
        var elt = document.getElementById(id);
        var grp = elt.parentNode;
        var newelt = elt.cloneNode(true);

        if (newelt.firstChild == null) {
          newelt.appendChild(document.createTextNode(lbl));
          } else {
          newelt.replaceChild(document.createTextNode(lbl), newelt.firstChild);
          }
        grp.replaceChild(newelt, elt);
        }

      function motion() {
        var now = new Date();
        var s = leadingZero( now.getHours());
        s = s + ":" + leadingZero( now.getMinutes());
        s = s + ":" + leadingZero( now.getSeconds());
        updateText( "timeField", s);
        setTimeout( "motion()", 1000);
        }
    ]]>
  </script>

  <title>Digital Timer</title>
  <g id="body">
    <text id="timeField" x="10" y="50" font-size="48" fill="blue" stroke="red" stroke-width="1">00:00:00</text>
  </g>
  <rect x="1" y="1" width="99%" height="98%" fill="none" stroke="black" stroke-width="1" />
</svg>` |

### Проста iнтерактивна програма.

| `<svg xmlns="http://www.w3.org/2000/svg" width="640" height="480" encoding="utf-8">
  <script type="text/ecmascript">
    <![CDATA[
      var timeField = document.getElementById( "timeField");

      function updateText( id, lbl) {
        var elt = document.getElementById(id);
        var grp = elt.parentNode;
        var newelt = elt.cloneNode(true);

        if (newelt.firstChild == null) {
          newelt.appendChild(document.createTextNode(lbl));
          } else {
          newelt.replaceChild(document.createTextNode(lbl), newelt.firstChild);
          }
        grp.replaceChild(newelt, elt);
        }

      function getMouseCoordinates( evt) {
        updateText( "xCoord", "x=" + evt.clientX);
        updateText( "yCoord", "y=" + evt.clientY);
        updateText( "button", "button=" + evt.button);
        updateText( "screenX", "screenX=" + evt.screenX);
        updateText( "screenY", "screenY=" + evt.screenY);
        updateText( "altKey", "altKey=" + evt.altKey);
        updateText( "ctrlKey", "ctrlKey=" + evt.ctrlKey);
        updateText( "shiftKey", "shiftKey=" + evt.shiftKey);
        }
    ]]>
  </script>

  <title>Coordinates</title>
  <g id="canvas" **onmousemove**="getMouseCoordinates( evt)" x="0" y="0" width="640" height="480">
    <rect x="1" y="1" width="99%" height="98%" fill="white" stroke="black" stroke-width="1" />
    <g id="display" font-size="48" fill="blue" stroke="red" stroke-width="1">
      <text id="xCoord" x="10" y="50">Empty</text>
      <text id="yCoord" x="10" y="100">Empty</text>
      <text id="button" x="10" y="150">Empty</text>
      <text id="screenX" x="10" y="200">Empty</text>
      <text id="screenY" x="10" y="250">Empty</text>
      <text id="altKey" x="10" y="300">Empty</text>
      <text id="ctrlKey" x="10" y="350">Empty</text>
      <text id="shiftKey" x="10" y="400">Empty</text>
    </g>
  </g>
</svg>` |

## Завдання:

1.  Намалювати секундомiр з двома кнопками “Start” та “Stop”. Забезпечити можливiсть його iнтерактивного запуску та зупинки.
2.  Намалювати свiтлофор. Забезпечити вiдповiдне перемикання свiтлових сигналiв.
3.  Намалювати метроном. Забезпечити вiдповiдний рух маятника. Повинна бути забезпечена можливiсть його вмикання та вимикання кнопкою.
4.  Намалювати вентилятор, гвинт якого повинен обертатися. Повинна бути забезпечена можливiсть його вмикання та вимикання кнопкою.
5.  Намалювати вентилятор, напрямок обертання гвинта якого можна змiнювати кнопками.
6.  Намалювати дитячу гойдалку. Забезпечити її рух та можливiсть керування за допомогою кнопок.
7.  Намалювати дитячу карусель. Забезпечити її рух та можливiсть керування за допомогою кнопок.
8.  Намалювати настольну лампу. Повинна бути забезпечена можливiсть її вмикання та вимикання кнопкою.
9.  Намалювати компас, стрiлка якого завжди вказує на "магнiтний" прямокутник, який можна рухати мишкою.
10.  Намалювати калькулятор, що виконує чотири основнi арифметичнiх операцiї та забезпечити його функцiональнiсть.
11.  Намалювати годинник. Його стрiлки повиннi рухатися, та завжди показувати вiрний час. Повинна бути забезпечена можливiсть його вмикання та вимикання кнопкою.
12.  Намалювати годинник з маятником. Забезпечити вiдповiдний рух маятника. Повинна бути забезпечена можливiсть його вмикання та вимикання кнопкою.
13.  Намалювати схему атома гелiя, електрони повиннi обертатися навколо ядра по рiзних елиптичних траєкторiях. Забезпечити можливiсть пересування ядра по малюнку мишкою.