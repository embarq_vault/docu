<a name="top"></a>

# Лабораторна робота №10.

## Тема: Використання тегу Canvas. Анiмацiя.

## Мета:

1.  **Опанувати особливостi роботи з тегом Canvas.**
2.  **Навчитися використовувати основнi функцiї Canvas для створення анiмацiйних ефектiв.**

## Теоретичнi вiдомостi

Тег `canvas` спецiально не призначено для анiмацiї. Тому для досягнення анiмацiйного ефекту доводиться кожного разу перемальовувати динамiчну сцену.

Головнi кроки анiмацiї є такими:

1.  Очистити `canvas`. Як правило, для цього використовується метод `clearRect`.
2.  Зберегти стан `canvas`. Це треба зробити у тому випадку, якщо у процесi малюваня графiчних примiтивiв потрiбно змiнити атрибути стилiв, параметри трансформацiї, тощо. В той час, як оригiнальнi параметри тегу `canvas` використовуються поза анiмацiєю.
3.  Намалювати примiтиви, що анiмуються.
4.  Вiдновити стан `canvas`. Якщо вiн був попередньо збережений.

Внутрiшня архiтектура сучасних броузерiв така, що сам процес малювання спостерiгати не можливо. Можна побачити тiльки його результат (пiсля кiнця процесу вiдтворення Web-сторiнки).

Контроль анiмацiї на Web-сторiнцi можна здiйснювати за допомогою функцiй `setInterval` i `setTimeout`.

Цi функцiї мають такий синтаксис:

1.  `setInterval( "animateShape()", 500);`
2.  `setTimeout( "animateShape()", 500);`

У якостi першого аргумента функцiї `setInterval` використовується iм'я функцiї, яка буде викликатися на виконання кожного разу при вичерпаннi промiжоку часу, вказаного, як другий параметр.

Функцiя `setTimeout` виконується лише один раз та через промiжок часу, вказаний у якостi другого параметру, викликає на виконання функцiю, iм'я якої вказано у якостi першого параметра.

Промiжок часу вказується у мiлiсекундах.

## Приклади анiмацiї

### Аналоговий годинник

| `<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <title>Clock Demo. Version 1.00</title>
    <script language="JavaScript">
      function init(){
        clock();
        setInterval( "clock()", 1000);
        }

      function clock() {
        var now = new Date();
        var ctx = document.getElementById('canvas').getContext('2d');
        ctx.save();
          ctx.clearRect( 0, 0, 200, 200);
          ctx.translate( 100, 100);

          ctx.lineWidth = 16;
          ctx.strokeStyle = "#77aadd";
          ctx.beginPath();
            ctx.arc( 0, 0, 90, 0, 2 * Math.PI, true);
          ctx.stroke();

          ctx.rotate( -Math.PI / 2);
          ctx.strokeStyle = "black";
          ctx.fillStyle = "white";
          ctx.lineWidth = 8;
          ctx.lineCap = "round";
          HourMarks( ctx);
          MinuteMarks( ctx);
          var sec = now.getSeconds();
          var min = now.getMinutes();
          var hr = now.getHours();
          hr = hr >= 12 ? hr - 12 : hr;

          // write Hours
          Arrow( ctx, (Math.PI / 6) * hr + (Math.PI / 360) * min + (Math.PI / 21600) * sec , "black", 5, 65);

          // write Minutes
          Arrow( ctx, (Math.PI / 30) * min + (Math.PI / 1800) * sec, "black", 5, 82);

          // Write seconds
          Arrow( ctx, sec * Math.PI / 30, "red", 3, 92);

        ctx.restore();
        }

      function HourMarks( ctx) {
        ctx.save();
        ctx.lineWidth = 3;
        for (i = 0; i < 12; i++) {
          ctx.beginPath();
            ctx.moveTo( 85,0);
            ctx.lineTo( 95,0);
          ctx.stroke();
          ctx.rotate( Math.PI / 6);
          }
        ctx.restore();
        }

      function MinuteMarks( ctx) {
        ctx.save();
        ctx.lineWidth = 3;
        for (i = 0; i < 60; i++) {
          if (i % 5 != 0) {
            ctx.beginPath();
              ctx.moveTo( 93, 0);
              ctx.lineTo( 95, 0);
            ctx.stroke();
            }
          ctx.rotate( Math.PI / 30);
          }
        ctx.restore();
        }

      function Arrow( ctx, angle, strokeStyle, lineWidth, arrowSize) {
        ctx.save();
          ctx.strokeStyle = strokeStyle;
          ctx.rotate( angle)
          ctx.lineWidth = lineWidth;
          ctx.beginPath();
            ctx.moveTo( -(0.25 * arrowSize), 0);
            ctx.lineTo( arrowSize, 0);
          ctx.stroke();
        ctx.restore();
        }
    </script>
  </head>
  <body bgcolor="#99ccff" onLoad="init()">
    <h1 align="center">Clock Demo. <br> Version 1.00</h1>
    <table border="1" align="center">
      <tr>
        <td>
          <canvas id="canvas" width="200" height="200" style="border: 1px dotted;">
            Резервний контент на випадок вiдсутностi пiдтримки полотна в браузерi.
          </canvas>
        </td>
      </tr>
    </table>
  </body>
</html>` |

[Результат роботи програми](http://192.168.100.19/%7Ewst/lw/html/lw_html_10/analogClock.html)

### Цифровий годинник

| `function clock() {
  var now = new Date();
  var ctx = document.getElementById('canvas').getContext('2d');
  ctx.save();
    ctx.clearRect( 0, 0, 300, 100);
    ctx.translate( ctx.canvas.width / 2, ctx.canvas.height / 2);
    ctx.scale( 3, 3);

    var sec = now.getSeconds();
    var min = now.getMinutes();
    var hr = now.getHours();

    ctx.font = "bold 20px sans-serif";
    ctx.fillStyle = "#5555ff";
    ctx.strokeStyle = "#ffff55";
    ctx.textAlign = "center";
    ctx.textBaseline = "middle";
    ctx.fillText( leadingZero( hr) + ":" + leadingZero( min) + ":" + leadingZero( sec), 0, 0);
    ctx.strokeText( leadingZero( hr) + ":" + leadingZero( min) + ":" + leadingZero( sec), 0, 0);
  ctx.restore();
  }

function leadingZero( s) {
  if (s < 10) {
    s = "0" + s;
    }
  return( s);
  }` |

[Результат роботи програми](http://192.168.100.19/%7Ewst/lw/html/lw_html_10/digitalClock.html)

### Приклади роботи з зображенями

1.  [Циклiчний рух малюнкiв](http://192.168.100.19/%7Ewst/lw/html/lw_html_10/image_motion.html).

## Завдання для самостiйної роботи

1.  Намалювати прямокутник, колiр якого змiнюється з часом.
2.  Намалювати зiрку, колiр якої змiнюється з часом.
3.  Намалювати колесо, що обертається з уповiльненням (i поступово зупиняється).
4.  Намалювати прямокутник, що обертається пульсуючи.
5.  Намалювати зiрку, розмiри якої змiнюються з часом (Зiрка повинна пульсувати).
6.  Намалювати колесо, колiр якого змiнюється з часом.
7.  Намалювати зiрку, що обертається з прискоренням (розкручується).
8.  Намалювати колесо, радiус якого змiнюється з часом (Колесо повинно пульсувати).
9.  Намалювати прямокутник, розмiри якого змiнюються з часом (Прямокутник повинен пульсувати).
10.  Намалювати колесо, що стрибає справа на лiво.
11.  Намалювати прямокутник, що рухається справа на лiво з уповiльненням.
12.  Намалювати зiрку, яка хитається справа на лiво.
13.  Намалювати колесо, що стрибає злiва на право.
14.  Намалювати прямокутник, що рухається знизу вверх з уповiльненням.
15.  Намалювати зiрку, яка хитається знизу вверх з прискоренням.
16.  Намалювати прямокутник, що рухається злiва на право.
17.  Намалювати зiрку, що рухається злiва на право з прискоренням.
18.  Намалювати колесо, що рухається зверху вниз з уповiльненням.
19.  Намалювати прямокутник, що рухається зверху вниз з прискоренням.
20.  Намалювати зiрку, що рухається зверху вниз з уповiльненням.
21.  Намалювати колесо, що рухається знизу вверх з прискоренням.