<a name="top"></a>

# Лабораторна робота №10.

## Тема: Asynchronous JavaScript and XML (AJAX).

## Мета:

1.  Опанувати основнi навички створення динамiчних HTML - документiв.
2.  Набути досвiд використаня технологiї асинхронного завантаження даних на Web - сторiнку.
3.  Дослiдити проблеми та особливостi створення Web 2.0 - додаткiв.

## Теоретичнi вiдомостi

### Уведення

Технологія Asynchronous JavaScript and XML (AJAX) вперше була реалізована компанією Microsoft, у браузері Internet Explorer 5.0 у вигляді об’єкту ActiveX, що доступний через JScript, VBScript, або інші скриптові мови, що підтримуються браузером. З часом програмісти проекту Mozilla розробили сумісну версію, що має назву XMLHttpRequest у Mozilla 1.0\. У подальшому ця можливість також була реалізована компаніями Apple починаючи з Safari 1.2, компанією Opera Software починаючи з Opera 8.0, браузером Konqueror, і, можливо, іншими.

Головним елементом технології AJAX є XMLHTTP (XMLHttpRequest) - набір API, що використовується у мовах JavaScript, JScript, VBScript та їм подібних для пересилки XML-даних по HTTP-протоколу між браузером и Web-сервером. Він дозволяє виконувати HTTP-запити до віддаленого сервера без необхідності перезавантаження сторінки.

XMLHTTP використовується багатьма сайтами для створення динамічних, додатків, що швидко реагують на запити користувачів. Наприклад XMLHTTP використовується такими сайтами як Gmail, Google Suggest, MSN Virtual Earth.

Нажаль, XMLHTTP працює тільки з файлами, що розташовані на тому ж домені, що і сторінка, що його використовує. Як і у випадку JavaScript, це зроблено у цілях безпеки (cross-site scripting).

### Технологія

Технологію створення асинхронних динамічних Web – додатків можна умовно розділити на дві складові частини:

1.  Асинхронне завантаження даних з сервера.
2.  Динамічна перебудова структури HTML – документу на боці клієнта.

Розглянемо їх детальніше.

#### Асинхронне завантаження даних з сервера.

Для того, щоб проілюструвати процес асинхронного завантаження даних доведеться створити два документи:

1.  PHP – скрипт, що генерує дані у форматі XML.
2.  HTML – документ, який за допомогою механізму XMLHTTP (XMLHttpRequest) завантажує XML – дані.

##### PHP - скрипт, що генерує данi у форматi XML

| `<?php
header( "Content-type: text/xml");
header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header( "Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header( "Cache-Control: no-store, no-cache, must-revalidate");
header( "Cache-Control: post-check=0, pre-check=0", false);
header( "Pragma: no-cache");

$d = time();
echo "<?xml version=\"1.0\" encoding=\"windows-1251\"?>\n";
?>
  <time_xml>
    <date_string><?php echo date( 'D M d H:i:s O Y', $d); ?></date_string>
    <date>
      <year><?php echo date( 'Y', $d); ?></year>
      <month><?php echo date( 'm', $d); ?></month>
      <day><?php echo date( 'd', $d); ?></day>
      <week_day><?php echo date( 'w', $d); ?></week_day>
    </date>
    <time>
      <hours><?php echo date( 'G', $d); ?></hours>
      <minutes><?php echo date( 'i', $d); ?></minutes>
      <seconds><?php echo date( 's', $d); ?></seconds>
      <milliseconds><?php echo date( 's', $d); ?></milliseconds>
      <timezone><?php echo date( 'O', $d); ?></timezone>
    </time>
  </time_xml>` |

##### Динамiчний HTML - документ, обробник XML - даних

| `<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=windows-1251">
    <title>AJAX Demo</title>
    <script language="JavaScript">

      function createXMLhttpObject() {
        XMLhttpObject = false;
        if (window.XMLHttpRequest) {
          XMLhttpObject = new XMLHttpRequest();
          } else {
          if (window.ActiveXObject) {
            try {
              XMLhttpObject = new ActiveXObject( "Msxml2.XMLHTTP");
              }
            catch(e) {
              XMLhttpObject = new ActiveXObject( "Microsoft.XMLHTTP");
              }
            }
          }
        return XMLhttpObject;
        }

      function DataLoader( data, ajax) {
        XMLhttpObject = createXMLhttpObject();
        if (!XMLhttpObject) {
          alert( "XMLhttpObject = null");
          return;
          }
        XMLhttpObject.open( "GET", data, ajax);
        XMLhttpObject.send( null);
        if (ajax == false) {
          try {
            var xmlData = XMLhttpObject.responseXML;
            callbackDataLoader( xmlData);
            }
          catch(e) {
            return;
            }
          } else {
          try {
            XMLhttpObject.onreadystatechange = function() {
              if (XMLhttpObject.readyState == 4) {
                if (XMLhttpObject.status == 200) {
                  var xmlData = XMLhttpObject.responseXML;
                  callbackDataLoader( xmlData);
                  }
                } else {
                return;
                }
              }
            }
          catch(e) {
            return;
            }
          }
        }

      function callbackDataLoader( xmlData) {
        document.getElementById( "year").innerHTML = xmlData.getElementsByTagName( "year")[0].childNodes[0].nodeValue;
        document.getElementById( "month").innerHTML = xmlData.getElementsByTagName( "month")[0].childNodes[0].nodeValue;
        document.getElementById( "day").innerHTML = xmlData.getElementsByTagName( "day")[0].childNodes[0].nodeValue;
        document.getElementById( "week_day").innerHTML = xmlData.getElementsByTagName( "week_day")[0].childNodes[0].nodeValue;
        document.getElementById( "hours").innerHTML = xmlData.getElementsByTagName( "hours")[0].childNodes[0].nodeValue;
        document.getElementById( "minutes").innerHTML = xmlData.getElementsByTagName( "minutes")[0].childNodes[0].nodeValue;
        document.getElementById( "seconds").innerHTML = xmlData.getElementsByTagName( "seconds")[0].childNodes[0].nodeValue;
        document.getElementById( "milliseconds").innerHTML = xmlData.getElementsByTagName( "milliseconds")[0].childNodes[0].nodeValue;
        document.getElementById( "timezone").innerHTML = xmlData.getElementsByTagName( "timezone")[0].childNodes[0].nodeValue;
        }

      function update() {
        var v = Math.round( 10000 * Math.random());
        document.getElementById( "update_string").innerHTML = (new Date()).toString();
        DataLoader( "time_xml.asp" + "?hash=" + v, false);
        window.setTimeout( "update();", 2000);
        }
    </script>
  </head>
  <body bgcolor="#99ccff" leftmargin=0 topmargin=0>
    <h1 align=center>Asynchronous JavaScript and XML</h1>
    <h2 align=center>Time Demo</h2>
    <h3 align=center><span id="update_string">Update</span></h3>
    <h3 align="center">Current Time</h3>
    <table border=1 align=center>
      <tr align="center" bgcolor="#77aadd">
        <td><b>Date</b></td>
        <td><b>Time</b></td>
      </tr>
      <tr>
        <td>
          <ul>
            <li>year: <span id="year">Empty</span></li>
            <li>month: <span id="month">Empty</span></li>
            <li>day: <span id="day">Empty</span></li>
            <li>week_day: <span id="week_day">Empty</span></li>
          </ul>
        </td>
        <td>
          <ul>
            <li>hours: <span id="hours">Empty</span></li>
            <li>minutes: <span id="minutes">Empty</span></li>
            <li>seconds: <span id="seconds">Empty</span></li>
            <li>milliseconds: <span id="milliseconds">Empty</span></li>
            <li>timezone: <span id="timezone">Empty</span></li>
          </ul>
        </td>
      </tr>
    </table>
    <script language="JavaScript">
      update();
    </script>
  </body>
</html>` |

РНР – документ за допомогою функції **time()** отримує системний час та подає його у вигляді XML – документу. При цьому дуже важливо, щоб сервер, а через нього і клієнт знав, що сгенеровані дані є **XML – даними**, та ці дані є **завжди актуальними**. Для цого використовуються заголовки **header(...)** на початку файла.

HTML – документ є значно складнішим, бо він повинен:

1.  Створити об’єкт – парсер XML – даних. Функція **createXMLhttpObject()**. При цьому треба мати н увазі, що такий об’єкт у різних браузерах створюеться по різному. У Internet Explorer для цього використовується об’єкт **Msxml2.XMLHTTP** або **Microsoft.XMLHTTP**, в залежності від версії. У браузері FireFox - **XMLHttpRequest()**.
2.  Завантажити XML – дані з сервера методом **GET**. Функція **DataLoader(...)**. Тут теж є певні особливості, бо різні XML – парсери працюють по різному.
3.  Відобразити отримані та перетворені дані у відповідних елементах HTML – сторінки. Функція **displayData()**. Для цього у HTML – коді динамічної Web – сторінки передбачені спеціальні теги – **span** у атрибут **innerHTML** яких відбувається запис динамічної інформації. Кожен з цих тегів має свій унікальний ідентифікатор за яким до нього виконується звертання.
4.  У подальшому регулярно завантажувати з сервера оновлені XML - дані перетворювати їх та оновлювати зміст тегів **span**. Функція **update()**.

Структура даних розпарсованого XML – документу зберігається у змінній **time_data**, яка отримує своє значення у функції **callbackDataLoader(...)**.

#### Динамiчна перебудова структури HTML - документу на боцi клiєнта.

Кожен елемент HTML – документу має властивість **innerHTML** через яку можна отримати доступ до внутрішньої структури цього елементу. Це дозволяє динамічно змінювати як інформаційну складову HTML – документу, так і його об’єктну ієрархію.

При розробці таких сторінок слід передбачити ті елементи, які у процесі перегляду їх клієнтом будуть відображати динамічний конвент. Зручніше за все для цього використовувати теги **span**, надаючи кожному з них ідентифікатор (**id**).

У подальшому до них можна буде звернутися за допомогою функції **getElementById()**, та переписати властивість **innerHTML**. Наприклад, так:

| `document.getElementById( "week_day").innerHTML =
    time_data[0].getElementsByTagName( "week_day")[0].text;
` |

### Проблема з кешуванням в Microsoft Internet Explorer

Internet Explorer кешує GET-запити. В даному випадку це є великою проблемою. Одним з рішень є використання методу POST, який ніколи не керується, але він призначений для інших операцій. Іншим рішенням є використання методу запита GET, що включає унікальний рядок для кожного запиту, як показано у прикладі нижче.

| `req.open("GET", "xmlprovider.php?hash=" + Math.random());` |

Або установкою заголовка Expires на минулу дату у серверному скрипті, який генерує зміст XML. У PHP це буде так:

| `header( "Expires: Mon, 26 Jul 1997 05:00:00 GMT" );
header( "Last-Modified: " . gmdate( "D, d M Y H:i:s" ) . " GMT" );
header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );` |

У сервлетах Java це буде так:

| `response.setHeader("Pragma", "no-cache");
response.setHeader("Cache-Control", "must-revalidate");
response.setHeader("Cache-Control", "no-cache");
response.setHeader("Cache-Control", "no-store");
response.setDateHeader("Expires", 0);` |

Інакше можна змусити об’єкт XMLHttpRequest завжди витягувати новий зміст, не використовуючи кеш так:

| `req.open("GET", "xmlprovider.php");
req.setRequestHeader("If-Modified-Since", "Sat, 1 Jan 2000 00:00:00 GMT");
req.send(null);` |

Важливо відмітити, що усі ці методики повинні використовуватися у випадку, коли кешування заважає. У інших випадках є кращім отримання переваг від швидкості при кешуванні, можливо комбінуючи його зі спеціально уточненими датами модифікації або іншими заголовками на сервері так, щоб використовувати кешуваня по максимуму без отримання поганих результатів.

## Завдання:

1.  Асинхронне завантаження даних з сервера.
    1.  Створити РНР - документ, який за допомогою функцiї **time()** отримує системний час та подає його у виглядi XML - документу.
    2.  Розмiстити його на вiддаленому серверi.
    3.  Розробити HTML - документ, що реалiзує асинхронне завантаження та вiдображення XML - даних часу з вiддаленого сервера.
2.  Динамiчна перебудова структури HTML - документу на боцi клiєнта.
    1.  Обрати таблицю iсходних даних згiдно Вашого варiанту.
    2.  Проаналiзувати її структуру.
    3.  По результатах цього аналiзу розробити iнформацiйну структуру XML - файлу.
    4.  Створити XML - файл розробленої Вами структури та заповнити його даними з iсходної таблицi.
    5.  Розмістити створений Вами XML - файл на віддаленому сервері.
    6.  Користуючись [наведеним зразком](http://192.168.100.19/%7Ewst/lw/lw_js/lw_int_ajax/29_fm_radio_ajax_firefox.html), розробити інтерактивний HTML – документ, який в асінхроному режимі отримує дані з віддаленого сервера, розбирає їх, та динамічно перебудовує свою структуру в залежності від структури даних XML- документу та дій користувача.
    7.  Забезпечити розбір та представлення XML – файлу не менше ніж на два рівня.

| **_Iндивiдуальнi варiанти завдань:_** |
| 

1.  [Газети](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/01_newspapers.html)
2.  [Магiстерськi програми](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/02_magister_prog.html)
3.  [Органiзацiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/03_organisation.html)
4.  [Бiблiотеки](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/04_library.html)
5.  [Офiцiйнi вiзити](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/05_summit.html)
6.  [Благодiйнi фонди](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/06_fond.html)
7.  [Мiнистерства](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/07_ministry.html)
8.  [Президенти](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/08_president.html)
9.  [Мiжурядовi органiзацiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/09_international_org.html)
10.  [Банкi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/10_bank.html)

 | 

1.  [Столицi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/11_capital.html)
2.  [Навчальнi заклади](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/12_education.html)
3.  [Транспорт](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/13_transport.html)
4.  [Спорт](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/14_sport.html)
5.  [Економiчнi звязки](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/15_trade_rel.html)
6.  [Збройнi сили](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/16_defence.html)
7.  [Економiчнi кризи](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/17_depression.html)
8.  [Вiйни](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/18_war.html)
9.  [Терористичнi акти](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/19_terror.html)
10.  [Релiгiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/20_religion.html)

 | 

1.  [Громадськi органiзацiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/21_communal_org.html)
2.  [Полiтичнi партiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/22_polit_party.html)
3.  [Мiжнароднi органiзацiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/23_international_org.html)
4.  [Мережа Дж.Сороса](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/24_soros.html)
5.  [Конфенцiї](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/25_conference.html)
6.  [Нацiональнi свята](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/26_holiday.html)
7.  [Науковцi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/27_scientist.html)
8.  [Програми обмiну](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/28_exchange_prog.html)
9.  [FM Радiо](http://192.168.100.19/%7Ewst/lw/internet/lw_int_xml/29_fm_radio.html)

 |

Приклад виконання роботи: **FM Радiо**.

*   [XML - документ](http://192.168.100.19/%7Ewst/lw/lw_js/lw_int_ajax/29_fm_radio.xml)
*   [Динамiчна HTML - сторiнка](http://192.168.100.19/%7Ewst/lw/lw_js/lw_int_ajax/29_fm_radio_ajax_firefox.html)