<a name="top"></a>

# Лабораторна робота №10.

## Тема: Обробка RSS - документiв.

## Мета:

*   Опанувати методику розбору RSS - документiв.
*   Опанувати процес семантичного аналiзу XML - сторiнок.
*   Опанувати основнi принципи структуризацiї текстової iнформацiї та представлення її у виглядi iєрархiчного графу.

## Уведення

RSS Скорочення RSS найчастiше розшифровують як **Really Simple Syndication** (простий спосiб збору (iнформацiї)). iснує ще декiлька розшифровок, всi вони не вiдображають сутi в повнiй мiрi.

Насправдi, **RSS це широковживаний стандарт для публiкацiї (та доставки) iнформацiї, а найбiльшим чином оновлень, для веб-сторiнок**.

Тобто, тi хто створюють iнформацiйне наповнення (наприклад, новиннi сайти) використовують цей стандарт для того щоб давати своїм читачам оновлення серед своїх новин або статей. Робиться це у виглядi текстового файлу. зробленого з використанням мови XML.

Файли у форматi RSS стандартизовано таким чином, що користувачi можуть легко видiлити з них, та використати для себе логiчно вiдокремленi шматки iнформацiї - наприклад назву повiдомлення. або автора, або час створення.

Робиться це за допомогою програм, що називаються **RSS-читачi** або **агрегатори**.

Тобто, цi логiчно окресленi шматки iнформацiї можна використати для того щоб створити своє наповнення, або передивлятись змiст сайта, не заходячи на нього браузером - особливо зручно це тому, що RSS-читачi показують чи вiдбулися на сайтi якiсь оновлення, чи нi. i все це робиться однаково для будь якого сайту, що дотримується стандарту RSS.

Самий мiнiмум користi, яку може отримувати споживач за допомогою RSS-читача - це переглядати велику кiлькiсть сайтiв за самий короткий час, який тiльки можливо.

Самий мiнiмум користi, який може отримувати той хто iнформацiю створює - це моментальне її розповсюдження серед тих хто пiдписався на RSS-файл, який ще часто називають RSS-feed (стрiчка RSS), без анонсiв по email або якимось iншим способом.

Потрiбно сказати що ця проста технологiя за останнi два роки буквально взiрвала уяви щодо того , як можна ефективно розповсюджувати iнформ-наповнення веб-сайтiв. Зараз важко знайти поважаючий себе англомовний сайт, що не надає RSS-feed

Провiднi українськi сайти пiдключилися до цiєї тенденцiї, i все бiльше з них також стали надавати RSS-стрiчки. У тих, хто цього н е робить, потрiбно вимагати такої послуги.

## RSS 2.0: Специфiкацiя

RSS - це рiзновид XML. Всi файли у форматi RSS повиннi вiдповiдати специфiкацiям XML 1.0, опис яких є на сайтi органiзацiї W3C(http://www.w3c.org/).

На самому початку RSS-документ повинен мати елемент <rss>з обов'язковим аттрибутом, що визначає версiю специфiкацiї формату.</rss>

Далi треба пiдключити тег <channel> з його описом i безпосередньо контентом.

#### Список обов'язкових елементiв тегу <channel>

| Тег | Опис | Приклад застосування. |
| title | Назва RSS-каналу. | GoUpstate.com News Headlines |
| link | Посилання на ваш Веб-сайт. | http://www.goupstate.com/ |
| description | Бiльш докладний опис каналу. | The latest news from GoUpstate.com,
a Spartanburg Herald-Journal Web site. |

#### Список додаткових елементiв тегу <channel>

| Тег | Опис | Приклад застосування. |
| language | Мова, на якiй написаний змiст контента. | en-us |
| copyright | Права на змiст контента даного каналу. | Copyright 2002, Spartanburg Herald-Journal |
| managingEditor | Адреса електронної пошти людини, вiдповiдальної за змiст контента. | geo@herald.com (George Matesky) |
| webMaster | Адреса електронної пошти людини, вiдповiдальної за технiчнi проблеми, що стосуються каналу. | betty@herald.com (Betty Guernsey) |
| pubDate | Дата публiкацiї контента каналу. Усi часи й дати в RSS вiдповiдають **Специфiкацiї Дати Часу RFC 822**, за винятком того, що рiк може бути описаний або двома знаками або чотирма. | Sat, 07 Sep 2002 00:00:01 GMT |
| lastBuildDate | Час останнього оновлення або змiни контента. | Sat, 07 Sep 2002 09:42:31 GMT |
| category | Визначення категорiй до яких вiдноситься канал. Їхнiй опис дотримується тих же правил, що й опис подэлемента <category> елемента <item>. | <category>Newspapers</category> |
| generator | Назва програми, що створює даний файл RSS. | MightyInHouse Content System v2.3 |
| docs | Посилання на ресурс, де описанi специфiкацiя формату файлу RSS вiдповiдного формату. | http://blogs.law.harvard.edu/tech/rss |
| cloud | Дозволяє визначити веб-сервис, що пiдтримує iнтерфейс rssCloud й який може бути здiйснений через HTTP-POST, XML-RPC or SOAP 1.1. | <cloud domain="rpc.sys.com" port="80" path="/RPC2" registerProcedure="pingMe" protocol="soap"/> |
| ttl | Показує скiльки хвилин залишилося до наступного оновлення каналу. | <ttl>60</ttl> |
| image | Логотип. Картинка, що символiзує канал. |   |
| rating | Рейтинг PICS для каналу. |   |
| textInput | Можна використати для створення форми пошуку або форми вiдправлення листiв. |   |
| skipHours | Пiдказка для ресурсiв, що використовують канал. Показує кiлькiсть годин, яку можна пропустити. |   |
| skipDays | Пiдказка для ресурсiв, що використовують канал. Показує кiлькiсть годин, яку можна пропустити. |   |

#### Список обов'язкових елементiв тегу <item>

| Тег | Опис | Приклад застосування. |
| title | Назва елементу. | GoUpstate.com News Headlines |
| link | Шлях (URL) до елементу. | http://www.goupstate.com/ |
| description | Короткий опис елементу. | The latest news from GoUpstate.com,
a Spartanburg Herald-Journal Web site. |
| author | Адреса електронної пошти автора елементу. |   |
| category | Список категорiй, до яких вiдноситься елемент. Таких елементiв може бути кiлька. |   |
| comments | Шлях (URL) до комметаря по елементу. |   |
| enclosure | Опис ЗМI, що вiдноситься до елементу. |   |
| guid | Унiкальний опис елемента. Це может бути або якийсь код або URL. |   |
| pubdate | Час публiкацiї елемента. |   |
| source | Джерело (канал) до якого вiдноситься елемент. |   |

## Приклад розбору та порiвняння RSS-файлiв версiї 2.0 за допомогою мови програмування JavaScript

| 

<pre>      var cmdArgs = WScript.Arguments;
      var fso = new ActiveXObject("Scripting.FileSystemObject");
      var xmldoc = new ActiveXObject("msxml");
      var arrayURL = new Array();
      var m = 0;

      var file = fso.CreateTextFile( cmdArgs(0) + ".txt", true, false);
      file.Write( "Begin parcing RSS links: \n");
      compareDocuments( cmdArgs(0), cmdArgs(1));
      file.Write( "End of parcing.\n");
      file.Close();

      function compareDocuments( docOld, docNew) {
          var i = 0;
          xmldoc.URL = docOld;
          parse( xmldoc.root);
          var oldURL = new Array();
          for (i = 0; i < arrayURL.length; ++i) {
              oldURL[i] = arrayURL[i];
              }
          m = 0;
          arrayURL.length = 0;
          xmldoc.URL = docNew;
          parse( xmldoc.root);
          var newURL = new Array();
          for (i = 0; i < arrayURL.length; ++i) {
              newURL[i] = arrayURL[i];
              }
          compareArrays( oldURL, newURL);
          }

      function parse( root) {
          var i = 0;
          if (root.type == 1) {
              if (root.parent.parent.tagName == "item".toUpperCase()) {
                  if (root.parent.tagName == "link".toUpperCase()) {
                      arrayURL[m++] = root.text;
                      }
                  }
              }
          if (root.children != null) {
              for (i = 0; i < root.children.length; ++i) {
                  parse( root.children.item( i));
                  }
              }
          return( arrayURL);
          }

      function compareArrays( oldStrArr, newStrArr) {
          var flag = 0;
          for (i = 0; i < newStrArr.length; ++i) {
              flag = 0;
              for (j = 0; j < oldStrArr.length; ++j) {
                  if (newStrArr[i] == oldStrArr[j]) {
                      flag = 1;
                      }
                  }
              if (flag == 0) {
                  file.Write( newStrArr[i] + "\n");
                  }
              }
          }
    </pre>

 |

Ця програма написана на мовi JavaScript для операцiйної системи MS Windows 2000 / MS Windows XP. Тут використовується дуже зручний механiзм створення та виконання скриптiв, що називається **Windows Scripting Host**. Цей механызм забезпечує читання програмою назв файлiв даних з командного рядка i створення ActiveX-об'єктiв для розбору XML-файлiв та збереження результатiв у вихiдному текстовому файлi.

У першому рядку програми
**var cmdArgs = WScript.Arguments;**
виконується читання аргументiв командного раядка та збереження їх до масиву **cmdArgs**.

У другому та третьому рядках
**var fso = new ActiveXObject("Scripting.FileSystemObject");
var xmldoc = new ActiveXObject("msxml");**
створюються ActiveX-об'єкти для роботи з файловою системою (**fso**) та розбора XML-файлiв (**xmldoc**).

Далi виконується створення текстового файлу
**var file = fso.CreateTextFile( cmdArgs(0) + ".txt", true, false);**
розбiр та порiвняння iсходних XML-файлiв та запис до нього результатiв розбору.

Розбiр деревовидної iнформацiйної структури XML-документу вiдбувається у функцiї **parse()**.

Порiвняння масивiв URL-посилань, що пiдготовленi у функцiї **parse()**, вiдбувається у функцiї **compareArrays()**.

Виклики функцiй **parse()** та **compareArrays()** для порiвняння двох XML-документiв виконуються у головнiй функцiї **compareDocuments()**.

Запустити цю програму на виконання можна таким чином:

1.  Створити окремий каталог, та скопiювати до нього RSS-файли, що треба порiвняти.
2.  За допомогою редактору **notepad** створити в цьому каталозi текстовий файл з розширенням **JS** та скопiювати до нього наведений вище приклад програми.
    Наприклад: **rss_compare.js**
3.  Вiдкрити вiкно командного режиму (Start->Run, у дiалоговому полi ввести команду **cmd**) та перейти до створеного Вами каталогу.
4.  З командного рядка запустити
    **N:\rss_dir>rss_compare.js [old.rss](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_4_old.xml) [new.rss](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_4.xml)**

5.  Програма **rss_compare.js** створить у цьому каталозi текстовий файл **old.rss.txt** i запише до нього список посилань на новини, яких не було у файлi **old.rss** i якi з'явилися у файлi **new.rss**.

Якщо файл iснує, то його можна вiдкрити у режимi читання таким чином:
**var file = fso.OpenTextFile("C:\\TEST\\test.txt", 2, true);**
Обов'язковим параметром тут є тiльки перший - iм'я файлу.
Другий параметер визначає режим вiдкриття файлу:
1 - файл вiдкриваєтся тiльки для читання;
2 - файл вiдкриваєтся для запису, його попереднiй змiст знищується;
8 - файл вiдкриваєтся для додавання даних, вони додаються у кiнець файлу.
Останнiй, третiй параметер визначає примусове створення файлу у тому разi, якщо його нема. Тобто, якщо вказати **true**, то файл буде створений обов'язково, а якщо вказати **false**, то файл створено не буде.

## Пошук по атрибутам елементiв RSS-файлiв версiї 2.0 можна здiйснити таким чином:

| 

<pre>      function parse( root) {
          var i = 0;
          if (root.type == 0) {
              if (root.tagName == "enclosure".toUpperCase()) {
                  if (root.parent.tagName == "item".toUpperCase()) {
                      arrayURL[m++] = root.GetAttribute( "url");
                      }
                  }
              }
          if (root.children != null) {
              for (i = 0; i < root.children.length; ++i) {
                  parse( root.children.item( i));
                  }
              }
          return( arrayURL);
          }
    </pre>

 |

## Завдання

1.  [Текстовий файл](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_01.txt) новин конвертувати до RSS - формату, вiдсортувавши **за авторами**.

2.  Створити RSS - файл з новинами категорiї "**Avia**", що є у [текстовому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_02.txt).

3.  Порiвняти два RSS - файли та вивести на екран **заголовки** новин, що з'явилися у бiльш [новому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml) (яких не було у [старому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2_old.xml)).

4.  Створити RSS - файл з новинами що вiдбулися у **парнi** дати. Данi взяти з [текстового файлу](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_01.txt).

5.  [Текстовий файл](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_02.txt) новин конвертувати до RSS - формату, вiдсортувавши **за категорiями**.

6.  Створити RSS - файл з новинами категорiї "**Human**", що є у [текстовому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_02.txt).

7.  Вивести на екран з RSS - файлiв [1](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_1.xml), [2](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml), [3](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_3.xml), **посилання** на файли новин, що датованi 18 лютого.

8.  Створити RSS - файл з новинами що вiдбулися у **непарнi** дати. Данi взяти з [текстового файлу](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_01.txt).

9.  Вивести на екран список **посилань на файли** новин, що є у [RSS - файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_1.xml).

10.  Створити RSS - файл з новинами категорiї "**Astro**", що є у [текстовому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/test_news_02.txt).

11.  Вивести на екран з RSS - файлiв [1](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_1.xml), [2](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml), [3](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_3.xml), **заголовки новин**, що датованi 18 лютого.

12.  Вивести на екран список **заголовкiв** новин, що є у [RSS - файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml).

13.  Порiвняти два RSS - файли та вивести на екран **посилання** на файли новин, що з'явилися у бiльш [новому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml) (яких не було у [старому файлi](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2_old.xml)).

14.  Розробити програму для об'єднання новин з RSS-файлiв [1](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_1.xml), [2](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml), [3](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_3.xml), При виконаннi об'єднання програма повинна вiдсортувати новини за датами публiкацiї.

15.  Розробити програму для об'єднання останнiх новин з RSS-файлiв [1](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_1.xml), [2](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_2.xml), [3](http://192.168.100.19/%7Ewst/lw/internet/lw_int_rss/lw_rss_3.xml), При виконаннi об'єднання програма повинна сформувати RSS-файл до якого включенi по двi найсвiжiшi новини з кожного початкового RSS-файлу.